package com.example.azzarnoy.geochat.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.azzarnoy.geochat.R;
import com.facebook.stetho.Stetho;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "myLogs";
    final String SAVED_TEXT = "saved_text";

    EditText name;
    Button btStart;
    SharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stetho.initializeWithDefaults(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();


        name = (EditText) findViewById(R.id.name_text_id);
        btStart = (Button) findViewById(R.id.start_bt_id);

        loadText();
        saveText();


    }

    //method for load saved nicknames or names from shared preference
    private void loadText() {
        sPref = getPreferences(MODE_PRIVATE);
        String savedText = sPref.getString(SAVED_TEXT, null);
        if (savedText == null) {
            Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(MainActivity.this, ActivityMap.class);
            intent.putExtra("login", savedText);
            startActivity(intent);
            Log.d(LOG_TAG, "name load from shared pref");

        }
    }

    //    method for save entered nicknames or names to shared preference
    private void saveText() {
        btStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sPref = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor ed = sPref.edit();
                ed.putString(SAVED_TEXT, name.getText().toString());
                ed.commit();
                Intent intent = new Intent(MainActivity.this, ActivityMap.class);
                startActivity(intent);
                Log.d(LOG_TAG, "name saved in shared pref");

            }
        });
    }


}
