package com.example.azzarnoy.geochat.modules;


public class User {
    public String text;
    public String lat;
    public String lng;
    public String user_id;

    public User(String text, String lat, String lng, String user_id) {
        this.text = text;
        this.lat = lat;
        this.lng = lng;
        this.user_id = user_id;


    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
