package com.example.azzarnoy.geochat.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.azzarnoy.geochat.R;
import com.example.azzarnoy.geochat.modules.User;

import java.util.List;

public class ActivityList extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();


        text = (TextView) findViewById(R.id.text_id);
        final ListView listView = (ListView) findViewById(R.id.listView);


        class MyAdapter extends ArrayAdapter<User> {

            public MyAdapter(Context context, List<User> objects) {
                super(context, R.layout.list_item, objects);
            }


            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder holder;
                View rowView = convertView;
                if (rowView == null) {
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    rowView = inflater.inflate(R.layout.list_item, parent, false);
                    holder = new ViewHolder();
                    holder.textOfUser = (TextView) rowView.findViewById(R.id.text_id);

                    rowView.setTag(holder);
                } else {
                    holder = (ViewHolder) rowView.getTag();
                }

                User user = getItem(position);
                holder.textOfUser.setText(user.getText());


                return rowView;
            }

            class ViewHolder {

                public TextView textOfUser;

            }


        }
    }
}
