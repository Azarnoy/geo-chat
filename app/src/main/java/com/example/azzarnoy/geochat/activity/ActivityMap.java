package com.example.azzarnoy.geochat.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.azzarnoy.geochat.R;
import com.example.azzarnoy.geochat.modules.Retrofit;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ActivityMap extends AppCompatActivity implements OnMapReadyCallback {

    private static final String LOG_TAG = "myLogs";

    final LatLng KHARKOV = new LatLng(49.9944422, 36.2368201);
    EditText etSend;
    Button btSend;
    Button btChange1;
    Button btChange2;
    Button btChange3;
    String userLogin;
    Retrofit.SafeTextApi sta;
    private LocationManager locationManager;
    Location location;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        btSend = (Button) findViewById(R.id.button_send_id);
        btChange1 = (Button) findViewById(R.id.button_map_change1_id);
        btChange2 = (Button) findViewById(R.id.button_map_change2_id);
        btChange3 = (Button) findViewById(R.id.button_map_change3_id);
        etSend = (EditText) findViewById(R.id.et_send_id);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//      send message to the api
        sendMess();

    }

    private void sendMess() {
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin = getIntent().getExtras().getString("login");
                String text = etSend.getText().toString();
//                String lat = String.valueOf(location.getLatitude());
//                String lng = String.valueOf(location.getLongitude());


                Retrofit.sendMessage(text, userLogin, new Callback<Void>() {
                    @Override
                    public void success(Void aVoid, Response response) {
                        Log.d(LOG_TAG, "text send to api");


                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.d(LOG_TAG, "text don't send to api");

                    }
                });


            }
        });
    }

    // load the map on KHARKOV point
    @Override
    public void onMapReady(final GoogleMap map) {

        Marker marker = map.addMarker(new MarkerOptions().position(KHARKOV).title("Kharkov"));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        map.moveCamera(CameraUpdateFactory.newLatLngZoom(KHARKOV, 13));
        map.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);
        map.setMyLocationEnabled(true);
        Log.d(LOG_TAG, "map is load");
//      set buttons to change the map view
        btChange1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
        });
        btChange2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
            }
        });
        btChange3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                map.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
            }
        });


    }


}



