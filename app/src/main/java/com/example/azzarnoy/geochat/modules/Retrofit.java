package com.example.azzarnoy.geochat.modules;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;


public class Retrofit {

    private static final String LOG_TAG = "myLogs";

    private static final String ENDPOINT = "http://psi.kh.ua";
    public static SafeTextApi safeTextApi;
    private static GetUserApi getUserApi;

    //interface for POST request to api
    public interface SafeTextApi {
        @FormUrlEncoded
        @POST("/hakaton/api.php")
        void safeTextApi(@Field("text") String text, @Field("lng") String lng, @Field("lat") String lat, @Field("user_id") String user_id, retrofit.Callback<Void> callback);
    }

    //interface for GET request from api
    interface GetUserApi {
        @GET("/hakaton/api.php")
        void getUserdata(@Query("text") String text, @Query("lng") String lng, @Query("lat") String lat, @Query("user_id") String user_id, retrofit.Callback<List<User>> callback);


    }


    static {
        init();
    }

    private static void init() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        safeTextApi = restAdapter.create(SafeTextApi.class);
    }

    //method for load data from api
    public static void getUserData(String text, String user_id, Callback<List<User>> callback) {
        getUserApi.getUserdata(text, user_id, "", "", callback);
    }

    //method for sending data to api
    public static void sendMessage(String text, String user_id, retrofit.Callback<Void> callback) {
        safeTextApi.safeTextApi(text, "22", "33", user_id, callback);

    }


}